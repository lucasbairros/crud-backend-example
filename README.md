# crud-backend-example

Welcome, this is a project for educational purposes, make good use of it my friend!

```
cd existing_repo
git remote add origin https://gitlab.com/lucasbairros/crud-backend-example.git
git branch -M main
git push -uf origin main
```

## Project setup
composer install

# Api url http://api.devzinho.com/api/

# store POST
```php
endpoint: store

objct data: { 
            'key' => {your-key},
            'data' => {}
           }
```
# update PUT

```php
endpoint: update/{key}

objct data: { 
            'data' => {id: {id-item}...}
           }
```
# edit GET
```php
endpoint: edit/{key}
```

# show GET
```php
endpoint: show/{key}
```
# delete GET
```php
endpoint: delete/{key} or
endpoint: delete/{key}/{id}
```
