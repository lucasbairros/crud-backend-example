<?php

$url = "http://api.devzinho.com/api/";
$key = "<your-key>";

// Define o cabeçalho da requisição
$headers = ['Accept' => 'application/json'];

// Define os parâmetros a serem enviados
$query = [
    'data' => [
        'id' => null,
        'text1' => 'TEST 1',
        'text2' => 'TEST 2'
    ]
];

// Executa a requisição, obtendo a resposta GET 
$response = Unirest\Request::post($url . 'edit/' . $key, $headers, $query);
var_dump($response);

// Executa a requisição, obtendo a resposta GET 
$response = Unirest\Request::get($url . 'edit/' . $key, $headers);
var_dump($response);

// Executa a requisição, obtendo a resposta GET 
$response = Unirest\Request::get($url . 'show/' . $key, $headers);
var_dump($response);
